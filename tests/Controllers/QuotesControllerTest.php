<?php

// tests/Controller/QuotesController.php
namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class QuotesControllerTest extends WebTestCase
{
  public function testGetQuotes()
  {
    $client = static::createClient();

    $client->request('GET', 'shout/steve-jobs?limit=2');

    $this->assertEquals(200, $client->getResponse()->getStatusCode());
  }


  public function testGetQuotesAppResponse()
  {
    $client = static::createClient();

    $client->request('GET', 'shout/steve-jobs?limit=2');
    $this->assertResponseHasHeader('Content-Type', 'application/json');
  }


  public function testGetQuotesNoLimit()
  {
    $client = static::createClient();

    $client->request('GET', 'shout/steve-jobs');

    $this->assertEquals(400, $client->getResponse()->getStatusCode());
  }

  public function testGetQuotesLimitGreaterThan10()
  {
    $client = static::createClient();

    $client->request('GET', 'shout/steve-jobs?limit=12');

    $this->assertEquals(400, $client->getResponse()->getStatusCode());
  }


  public function testGetQuotesCheckShoutingHasExclamation()
  {
    $client = static::createClient();

    $client->request('GET', 'shout/steve-jobs?limit=2');
    $cont = $client->getResponse()->getContent();
    $cont = json_decode($cont);


    $this->assertEquals("!", substr($cont[1], -1));
  }

  public function testGetQuotesCheckShoutingCheckUppercase()
  {
    $client = static::createClient();

    $client->request('GET', 'shout/steve-jobs?limit=2');
    $cont = $client->getResponse()->getContent();
    $cont = json_decode($cont);

    $this->assertStringContainsString(strtoupper($cont[1]), $cont[1]);
  }

}