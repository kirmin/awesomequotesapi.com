<?php
namespace App\Tests\Classes;

use App\Quotes\JsonQuotesProvider;
use App\Quotes\TheysaidsoQuotesProvider;
use App\Quotes\QuotesFactory;
use PHPUnit\Framework\TestCase;

class php extends TestCase
{


  public function testFactoryObjectTypeForJsonQuotesProvider()
  {
    $json_quotes_provider = QuotesFactory::getProvider(QuotesFactory::PROVIDER_JSON);

    $this->assertTrue($json_quotes_provider instanceof JsonQuotesProvider);
  }

  public function testFactoryObjectTypeFor()
  {
    $json_quotes_provider = QuotesFactory::getProvider(QuotesFactory::PROVIDER_THEY_SAID_SO);

    $this->assertTrue($json_quotes_provider instanceof TheysaidsoQuotesProvider);
  }

}