<?php

namespace App\Quotes;


use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Component\HttpClient\HttpClient;

class TheysaidsoQuotesProvider extends QuotesProvider
{

  public function getQuotes()
  {
    $famous_person_name = $this->getFamousPersonName();
    $limit = $this->getLimit();

    $ret = [];

    /**
     * @TODO add this to a global level and in config file  ...
     * // Added this cache dir so that it works on my local beucase it did not work with the default
     */
    $cache_dir = __DIR__ . '/../../var/cache/app_cache/';
    $cache = new FilesystemAdapter('', 0, $cache_dir);

    $ret = $cache->get(
      $famous_person_name . '_' . $limit.'_TheysaidsoQuotesProvider',
      function (ItemInterface $item) {
        $item->expiresAfter(3600);

        $limit = $this->getLimit();
        $famous_person_name = $this->getFamousPersonName();
        $num_quotes = 0;
        $ret = [];



        // Load data from https://theysaidso.com/api/


        $client = HttpClient::create();
        $response = $client->request('GET', 'http://quotes.rest/qod.json?category=inspire');

        $statusCode = $response->getStatusCode();
        // $statusCode = 200
        $contentType = $response->getHeaders()['content-type'][0];
        // $contentType = 'application/json'
        $content = $response->getContent();
        // $content = '{"id":521583, "name":"symfony-docs", ...}'
        $content = $response->toArray();
        // $content = ['id' => 521583, 'name' => 'symfony-docs', ...]

//        @TODO Implement logic here based on the above api return data and  provided parameters


        return $ret;
      }
    );

    return $ret;
  }


}

