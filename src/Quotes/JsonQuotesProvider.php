<?php

namespace App\Quotes;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Contracts\Cache\ItemInterface;

use Symfony\Component\Cache\Adapter\MemcachedAdapter;

class JsonQuotesProvider extends QuotesProvider
{

  public function getQuotes()
  {
    $famous_person_name = $this->getFamousPersonName();
    $limit = $this->getLimit();
    $ret = [];

    /**
     * @TODO add this to a global level and in config file  ...
     * // Added this cache dir so that it works on my local because it did not work with the default
     */
    $cache_dir = __DIR__ . '/../../var/cache/app_cache/';
    $cache = new FilesystemAdapter('', 0, $cache_dir);

    $ret = $cache->get(
      $famous_person_name . '_' . $limit,
      function (ItemInterface $item) {
        $item->expiresAfter(3600);

        $limit = $this->getLimit();
        $famous_person_name = $this->getFamousPersonName();
        $num_quotes = 0;
        $ret = [];
        $json_file = __DIR__ . '/data/quotes.json';
        if (file_exists($json_file)) {
          $quotes_json = file_get_contents($json_file);
          if (!empty($quotes_json)) {
            $quotes_data = json_decode($quotes_json);

            if (isset($quotes_data->quotes)) {
              foreach ($quotes_data->quotes as $key => $quote) {
                if ($num_quotes >= $limit) {
                  break;
                }

                if ($famous_person_name == strtolower($quote->author)) {
                  $ret[] = $this->shout($quote->quote);
                  $num_quotes++;
                }
              }
            }
          } else {
            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, "Quotes data is empty!");
          }
        } else {
          throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, "Failed to load quotes data!");
        }

        return $ret;
      }
    );

    return $ret;
  }


}
