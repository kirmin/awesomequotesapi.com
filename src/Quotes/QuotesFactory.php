<?php

namespace App\Quotes;


class QuotesFactory
{

  const PROVIDER_JSON = 'Json';
  const PROVIDER_THEY_SAID_SO = 'Theysaidso';

  static $provider;

  public static function getProvider($provider)
  {
    $providerName = __NAMESPACE__.'\\'.$provider.'QuotesProvider';
    if (class_exists($providerName)) {
		self::$provider = new $providerName;

      	return self::$provider;
    } else {
      // throw new Exception('Provider is not implemented!');
    }
  }


}
