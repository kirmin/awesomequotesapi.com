<?php

namespace App\Quotes;


abstract class QuotesProvider
{
	protected $famous_person_name;
	protected $limit;

	protected function shout(string $quote){
		return strtoupper($quote).'!';
	}


	public function setFamousPersonName(string $famous_person_name){
		// here we can do other types of validation for the name with regex or similar if we want to ...
		$famous_person_name = str_replace('-', ' ' , $famous_person_name);
		$famous_person_name = strtolower($famous_person_name);

		$this->famous_person_name = $famous_person_name; 
	}

	public function getFamousPersonName(){
		return $this->famous_person_name;
	}

	public function setLimit(int $limit){
		$this->limit=$limit;
	}

	public function getLimit(){
		return $this->limit;
	}

	abstract public function getQuotes();
}