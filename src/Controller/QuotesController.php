<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Quotes\QuotesFactory;
use Symfony\Component\HttpKernel\Exception\HttpException;


class QuotesController extends AbstractController
{

  /**
   * @Route("/shout/{famous_person_name}", methods={"GET"}, requirements={"famous_person_name"="[a-zA-Z-]+"}, defaults={"_format"="json"} )
   *
   */
  public function getQuotes(Request $request, $famous_person_name)
  {
    $limit = $request->query->get('limit');

    try{
      if (!intval($limit)) {
        throw new HttpException(
          Response::HTTP_BAD_REQUEST,
          "Please provide the limit parameter which must be a number from 1 to 10"
        );
      }

      if ($limit > 10) {
        throw new HttpException(Response::HTTP_BAD_REQUEST, "Limit must not exceed 10");
      }

      // Get Quotes Provider from Factory
      $json_quotes_provider = QuotesFactory::getProvider(QuotesFactory::PROVIDER_JSON);

      // We can easily switch to the other quotes provider because they use the same interface and methods
      // $json_quotes_provider = QuotesFactory::getProvider(QuotesFactory::PROVIDER_THEY_SAID_SO);

      // Get Quotes for the specified famous person and limit
      $json_quotes_provider->setFamousPersonName($famous_person_name);
      $json_quotes_provider->setLimit($limit);


      // Get data from provider
      $quotes = $json_quotes_provider->getQuotes();
    }catch (HttpException $e){
      return $this->json($e->getMessage(), $e->getStatusCode());
    }

    // Return json result
    return $this->json($quotes, Response::HTTP_OK);
  }


}